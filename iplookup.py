from ipdata import ipdata
import json
from settings import IP_api_key



def Lookup(ip):

    ipvar = ipdata.IPData(IP_api_key)

    try:
        response = ipvar.lookup(ip, fields=['ip','city','region_code','region','country_name'])
        #response = json.loads(urllib.request.urlopen(response).read().decode())
    except:
        return False  # In the case of an error, pass all IP's to avoid blocking innocents

    return response  # Defaults to None if failed to get block value