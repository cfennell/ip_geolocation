from argparse import Namespace

import iplookup
import csv
import pandas as pd
from pandas.io.json import json_normalize
import json
import argparse
import time
import os

def parse_arguments():
    parser = argparse.ArgumentParser(description='Check Source for IP addresses from a CSV file')

    parser.add_argument('-f', '--filename', action='store', help="Name of the file that conatins the IP addresses")

    params = parser.parse_args()
    print(params)
    return params


def main():

    info = parse_arguments()

    df = pd.read_csv(info.filename)
    ip_addresses = df[['IPAddress']]

    # Blank list for the Json results we get back from the API
    ipdictionary = pd.DataFrame()

    for ip in ip_addresses['IPAddress']:
        json_ip = iplookup.Lookup(ip)
        if json_ip == False:
            print('Not a Valid IP')
        elif json_ip != False:
            json_ip = json_normalize(json_ip)
            print(json_ip)
            
            #ipdictionary.append(json_ip)
            ipdictionary = ipdictionary.append(json_ip, ignore_index=True)

    # Add all ips json into one pandas dataframe
    totalIPS = pd.DataFrame.from_dict(ipdictionary)
    totalIPS.join(df)
    #totalIPS = json_normalize(totalIPS, 'ip', 'block')

    # Write to CSV
    totalIPS.to_csv('IP address ' + time.strftime("%Y%m%d-%H%M%S") + ".csv", sep=',')

    # Code for appending to CSV file
    #TODO: Finish this so it appends to the CSV

if __name__ == "__main__":
    #execute only if run as a script
    main()