# IP_GeoLocation

## Purpose

The goal of this project is to validate IP addresses from online
surveys. There are many reasons to validate IP addresses but this code
returns geo location data for ip addresses. It is a helpful tool. 

## How it works

The python script uses an API service hosted by IP Info
<https://ipdata.co/>. The script calls the API and receives a json
object in return with the following information

* Information returned in JSON
    * IP address
    * City
    * Region code - (State abbr.)
    * Region - (State Name)
    * Country
    * Postal Code

ipdata.co has far more fields to use including tor indicators, proxy, organization info. Additional fields can be added by modifying iplookup.py.

## Running the script

To run this script you should be using Python 3.x and have some idea how to run a python script from the command line. This python script uses the IPdata.co package. To install use pip to install the package to your python directory.

```
pip3 install ipdata
```

The script has one
argument

* -f --filename - the file name that contains the IP Addresses column. The column
    title needs to be in the first row and titled "IPAddress"
 
To run use the following in terminal

```
python main.py -f "myfile.csv"
```
OR
```
python main.py --filename "myfile.csv"
```
## Output

The output of the program will be placed in the same directory of the python program with the title IP Address + *[timestamp]*.csv

## Citation

Please cite this project in publications as:

```
Fennell, Chris (2019). IP Geolocation: Geographic data from IP addresses. Python package. <https://gitlab.msu.edu/cfennell/ip_geolocation>
```

or in bibtex

```
  @Manual{,
    title = {IP Geolocation: Geographic data from IP addresses},
    author = {Fennell, Chris},
    year = {2019},
    organization = {Michigan State University - Department of Media and Information},
    address = {East Lansing, MI,
    url = {https://gitlab.msu.edu/cfennell/ip_geolocation},
  }
```
## TODOs

